export interface IReactCMTemplate {
    name: string;
    path: string;
    outDir: string;
    subDir?: boolean;
}